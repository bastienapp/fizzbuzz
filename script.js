// while : tant que la condition est vraie, alors
// for : pour une valeur donnée, tant que la condition sur la valeur est vrai, alors
// for .. of : pour chaque valeur d'une tableau
/*
let step = 0;
while (step < 100) {
  step = step + 1; // step++;
  // vérifier si step est un multiple de 3 : modulo
  // un nombre a est un multiple de b, si le reste de la division entière entre a et b est égal à zéro
  // a % b === 0
  if (step % 3 === 0) {
    console.log("Fizz");
  } else {
    console.log(step);
  }
}

 6 | 2
___|__
 0 | 3
   |
*/

// portée d'un variable, scope
// une variable let ou const existe entres deux accolades uniquement

// déclaration de la fonction
function fizzBuzz(step) { // step : prend la valeur de i passé en argument
  if (dividedBy3(step)
    && dividedBy5(step)
  ) {
    console.log(step, "FizzBuzz");
  } else if (dividedBy3(step)) {
    console.log(step, "Fizz");
  } else if (dividedBy5(step)) {
    console.log(step, "Buzz");
  } else {
    console.log(step);
  }
}

// retourne vrai si le nombre peut être divisé par 3
// retourne faux sinon
function dividedBy3(number) {
  // vérifie si un nombre est divisible par 3
  if (number % 3 === 0) {
    return true;
  } else {
    return false;
  }
}

function dividedBy5(number) {
  // vérifie si un nombre est divisible par 5
  if (number % 5 === 0) {
    return true;
  } else {
    return false;
  }
}

// adaptée au parcour de tableau ou incrémentation d'une variable
for (let i = 1; i <= 100; i++) {
  // appel de la fonction
  fizzBuzz(i);
}

/*
// éviter les conditions imbiquées
if () {
  if () {

  }
}
*/